from setuptools import setup
def readme():
	with open('README.rst') as f:
		return f.read()
setup(
	name='first',
	version='1.0',
	description='First command line script',
	long_description=readme(),
	classifiers=[
		'Development Status :: Alpha',
		'Programming Language :: Python',
	],
	keywords='first try',
	url='https://bitbucket.org/karthick1996/first/',
	author='Karthick',
	author_email='rkarthick31996@gmail.com',
	license='MIT',
	packages=['first'],
	install_requires=[
		'markdown',
	],
	entry_points={
		'console_scripts':['first=first.command_line:main'],
	},
	include_package_data=True,
	zip_safe=False)
